package MR.MR;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCount {
		

 public static void main(String[] args) throws Exception {

     Configuration conf = new Configuration();
     Job job = new Job(conf, "wordcount");
     job.setJarByClass(WordCount.class);

     job.setMapperClass(WordCountMapper.class);
     job.setMapOutputKeyClass(Text.class);
     job.setMapOutputValueClass(IntWritable.class);
     FileInputFormat.addInputPath(job, new Path(args[0]));

     job.setOutputKeyClass(Text.class);
     job.setOutputValueClass(IntWritable.class);
     job.setReducerClass(WordCountReducer.class);
     FileOutputFormat.setOutputPath(job, new Path(args[1]));

     job.setNumReduceTasks(2);
     job.waitForCompletion(true);
   }
}