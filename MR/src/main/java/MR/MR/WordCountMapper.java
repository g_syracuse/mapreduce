package MR.MR;

import java.io.IOException;
import java.util.StringTokenizer;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
//import org.w3c.dom.Text;

public class WordCountMapper extends
Mapper<Object, Text, Text, IntWritable> {
private final IntWritable ONE = new IntWritable(1);
private Text word = new Text();
public void map(Object key, Text value, Context context)
    throws IOException, InterruptedException {
String line = value.toString();
StringTokenizer tokenizer = new StringTokenizer(line);
while(tokenizer.hasMoreTokens()) {
    word.set(tokenizer.nextToken());
    context.write(word, ONE);
}
}
} 

